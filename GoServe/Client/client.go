package main

import (
	"fmt"
	"net/http"
	"io/ioutil"	
	"os"
	"bytes"
	"time"
	"math/rand"
	"mime/multipart"
	"sync"
	"strconv"
	"log"
	_"crypto/tls"
)
 
 var waitGroup sync.WaitGroup

 // writeLines takes in a byte array and a filename or the 
 // path to the filename and writes the data in the byte
 // array into the file. It returns an error if it fails 
 // to write in the file.
 func writeLines(lines string, path string) (err error) {

	var (
	file *os.File
	)

	if file, err = os.Create(path); err != nil {
		return
	}

	defer file.Close()

	item := string(lines[:])
	file.WriteString(lines + "\n");  

	return
}

 // checkConn accepts the server hostname and establishes
 // a connection with it. It can setup either HTTP or HTTPS 
 // connection with cetificate nad key received from the 
 // server. It returns the http *CLient variable to send
 // further requests to the server.
 func checkConn(hostname string) *http.Client {

	cert, err := tls.LoadX509KeyPair("cert.pem", "key.pem")
		 if err != nil {
        	log.Println("Error in loading keys: %s", err)
    	}
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{Certificates: []tls.Certificate{cert}, InsecureSkipVerify: true},
	    }

	//client := &http.Client{}
	client := &http.Client{Transport: tr}

	log.Println("Trying to connect to " + hostname + "...")

	res, err := client.Get(hostname)

    	if err != nil {
		log.Println("Connection error with Server")
		os.Exit(1)
		return nil
	}
	
	log.Println("Connected. Connection status code: " + res.Status)

	defer res.Body.Close()
	
	return client
}

 // randomLocation randomly generates latitude and longitude
 // and a number for selecting an action later on. The latitutde
 // are ranged from 0 to 90 degrees and longitude from 0 to 180
 // degrees. Both positive and negative locations. It gives a 3
 // point decimal precision.
 func randomLocation () (float64,float64,int) {

	lat := randInt(0, 91)
	long := randInt(0, 181)

	decimalLat := randInt(1,1000)
	decimalLong := randInt(1,1000)	

	latitude := lat + (decimalLat/1000.0)
	longitude := long + (decimalLong/1000.0)

	operation := int(randInt(0, 6))
	
	switch randInt(0,4) {
		case 0: 
		case 1: latitude = 0 - latitude
		case 2: longitude = 0 - longitude
		case 3: latitude = 0 - latitude 
				longitude = 0 - longitude
		}
    
     	return latitude,longitude,operation
}

 // randInt is a helper function for randomLocation. It gives
 // a random number between the received maximum and minimum
 // range.
 func randInt(min int , max int) float64 {

    	return float64(min + rand.Intn(max-min))
}

 // getLatLong operates on the randomLocation to receive the
 // random coordinates and forms a string out of the received
 // values. It returns string values for the random location 
 // and integer for the number for action.
 func getLatLong() (string,string,int) {

	rand.Seed(time.Now().UTC().UnixNano())
	
	latitude,longitude,operation := randomLocation()

	latString := fmt.Sprintf("%g",latitude)
	longString := fmt.Sprintf("%g",longitude)

	return latString, longString, operation
}

 // formPostRequest takes in the hostname, latitude and longitude
 // and a number for the action, and creaets an HTTP POST request
 // after putting the values at their respective fields in the 
 // form hosted by the server. It return the HTTP request variable 
 // to send it to the server.
 func formPostRequest(hostname, latString, longString string, operation int) (*http.Request) {

	buf := new(bytes.Buffer) 
    	w := multipart.NewWriter(buf) 

    	err := w.WriteField("latitude", latString)
    		if err != nil {
                log.Println("Fatal error 'Latitude'")
                return nil
        	}
    
     	err = w.WriteField("longitude", longString)
    		if err != nil {
                log.Println("Fatal error 'Longitude'")
                return nil
       		}
	    
	var resource string
	switch operation {

		case 0: resource = "maps"

		case 1: resource = "wiki"

		case 2: resource = "flickr"

		case 3: resource = "flickr_nearby"

		case 4: resource = "address"

		case 5: resource = "resource"
	}	
	
	err = w.WriteField("select", resource)
    		if err != nil {
                log.Println("Fatal error 'Select' ")
                return nil
       		}		
    
      	w.Close() 
      	request, err := http.NewRequest("POST", hostname, buf)//GET for fetching the webpage
      		if err != nil {
                log.Println("Fatal error 'New Request' ")
                return nil
        	}

      	request.Header.Set("Content-Type", w.FormDataContentType())
	
	log.Println("Location sent: " + latString + " " + longString)

	return request
}

 // submitRequest takes in client and request and sends the request
 // to the server using the client variable. It receives a response 
 // from the server which it returns as HTTP response variable.
 func submitRequest (client *http.Client, req *http.Request) *http.Response {

      	response, err := client.Do(req)
      		if err != nil {
                log.Println("Fatal error 'Response'")
                return nil
        	}

	log.Println("Redirected with status code: " + response.Status)
	
	return response
}

 // writeResponse takes in the HTTP response and writes the
 // response in an external file to save it.
 func writeResponse(response *http.Response, i int) {

	location, err := ioutil.ReadAll(response.Body)
	    if err != nil {
                log.Println("Fatal error 'Read Response'")
                return
        	}

	writeLines(location, fmt.Sprintf("Response%d.html", i))
}

 // startRequests does the operation of starting out to
 // give request to a server by following the steps
 // one by one. It issues waitGroup.Done() when it is 
 // finished to decrement the wait group counter.
 func startRequests(hostname string, i int) {

	client := checkConn(hostname)

	latString, longString, operation := getLatLong()

	req := formPostRequest(hostname+"data.html", latString, longString, operation)
      	
	//submitRequest(client, req)	
	resp := submitRequest(client, req)
	
	writeResponse(resp, i)
	
	waitGroup.Done()
	
}

 // start buffers up requests for the server and adds
 // a wait group up to the number of requests issued
 // It starts a go rountine for each request. It then
 // waits for the all the request to complete before
 // exiting the program.
 func start(hostname string, requests int) float64 {

	waitGroup.Add(requests)  

	startTime := time.Now()
	
	for i:=0 ; i<requests ; i++ {

	go startRequests(hostname, i)
	
	}

	waitGroup.Wait()

	time := time.Since(startTime).Seconds()
	log.Println(fmt.Sprintf("%d requests completed in: %s", requests, strconv.FormatFloat(time, 'f', 4, 64)))

	return time
			
}

 // Declares the server hostname and the number
 // of requests to be thrown at the server. 
 // It then starts function to buffer the
 // requests via goroutines.
 func main() {
	
	//hostname := "http://localhost:8080/"
	hostname := "https:localhost:8080/"

	var count float64

	for requests:=1 ; requests<10000; requests = requests*10 {
	time := start(hostname, requests)
	count = count + time
	}

	log.Println(fmt.Sprintf("Average: %s", strconv.FormatFloat(count/float64(requests), 'f', 4, 64)))
}
