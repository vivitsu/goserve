package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"github.com/garyburd/redigo/redis"
	"bufio"
	"bytes"
	"math"
	"encoding/json"
	"crypto/md5"
	"html/template"
	"strings"
	"log"
	"time"
	_ "net/http/pprof"
)

const httpPort = ":8080"

 // Structure for parsing the Google Maps
 // JSON data, received from Google maps
 // API.
 type MapData struct {
	Data []Results `json:"results"`
	StatusCode string `json:"status"`
	}

 type Results struct {
	AddressComp []AddressComponents `json:"address_components"`
	FormattedAdd string `json:"formatted_address"`
	Geometry Geometry `json:"geometry"`
	PostalCode string `json:"postal_code"`
	}

 type AddressComponents struct {
	LongName string `json:"long_name"`
	ShortName string `json:"short_name"`
	}
 
 type Geometry struct {
	Bounds Bounds `json:"bounds"`
	Location Location `json:"location"`
	LocationType string `json:"location_type"`
	ViewPort ViewPort `json:"viewport"`
	}
 
 type Location struct {
	Latitude float64 `json:"lat"`
	Longitude float64 `json:"lng"`
	}

 type Bounds struct {
	NorthEast NorthEast `json:"northeast"`
	SouthWest SouthWest `json:"southwest"`
	}
	
 type ViewPort struct {
	NorthEast NorthEast `json:"northeast"`
	SouthWest SouthWest `json:"southwest"`
	}
	
 type NorthEast struct {
	Latitude float64 `json:"lat"`
	Longitude float64 `json:"lng"`
	}	

 type SouthWest struct {
	Latitude float64 `json:"lat"`
	Longitude float64 `json:"lng"`
	}

 // Struct for latitude and longitde
 // data which are stored in degrees
 // decimal and direction.
 type coordinate struct {
	degrees int64
	decimal int64
	direction string
	}

 // To send the correct user key in HTML
 // files, so that the user can be properly
 // redirected. Also includes the error 
 // string to be displayed. The history
 // byte array is used to print history.
 type User struct {
	Name string
	UserHash string
	Action string
	Error string
	History []byte
	}
	  	
	// Shows up the main screen from where the user selects
	// the options.
	func GetUserInput(w http.ResponseWriter, req *http.Request) {

		http.ServeFile(w, req, "html/splash.html")
	}

	// Denies access to any particular requests which are
	// not valid.
	func DenyAccess(w http.ResponseWriter, req *http.Request) {
	
		http.ServeFile(w, req, "html/denyaccess.html")
	}

	// Redirects the user to any of the screens it clicks to.
	// If no option is selected then it invalidates request 
	// and refreshes to the home page
	func RedirectUser(w http.ResponseWriter, req *http.Request) {

		actionSelected := req.FormValue("redirect")

		switch actionSelected {
	
			case "register":
		
				http.ServeFile(w, req, "html/register.html")

			case "signin":

				http.ServeFile(w, req, "html/signin.html")

			case "guest":

				http.ServeFile(w, req, "html/guest.html")

			default:

				http.ServeFile(w, req, "html/denyaccess.html")

		}
	}

	// Redirects the user from its account to either search something
	// or view its location history or else log out of the system.
	// Validates the user everytime it makes any choices!
	func RedirectFromAccount(w http.ResponseWriter, req *http.Request) {
	
		c, err := redis.Dial("tcp", ":6379")		
		if err != nil {
	    		log.Println("Error in connecting to REDIS Server ", err.Error())
	    		return
		}

		defer c.Close()

		userHash := req.FormValue("userhash")

		userNameHash, err := c.Do("HGET", userHash, "Name")
		if err != nil {
			log.Println("Error ", err.Error(), " in fetching value from REDIS. Quitting...")
			return
		}

		userName, err := redis.String(userNameHash, nil) 
			if err != nil {
			log.Println("Error in converting to String ", err.Error())
			return 
		}

		actionSelected := req.FormValue("account")


		if userHash == "" {

			http.ServeFile(w, req, "html/denyaccess.html")
			return
		}

		switch actionSelected {
	
			case "history":
		
				RetrieveHistory(w, userHash)				

			case "search":

				if userName == "" {

					http.ServeFile(w, req, "html/denyaccess.html")
					return
				}

				t, err := template.ParseFiles("html/user.html")
				if err != nil {
					log.Println("Template parsing error ", err.Error())
					return
				}

				n := &User{Name: userName, UserHash: userHash}
				t.Execute(w, n)

			case "signout":

				t, err := template.ParseFiles("html/redirection.html")
				if err != nil {
					log.Println("Template parsing error ", err.Error())
					return
				}

				n := &User{Action: "Sign-out"}
				t.Execute(w, n)
		
			default:

				http.ServeFile(w, req, "html/denyaccess.html")

		}
	}
		
	// Checks the registration form filled up by the user and validates
	// the account if the information entered is correct and error free.
	// Shows errors if user leaves any required field empty or doesn't 
	// conform to the rules of registration. Also validates the if the
	// user id is unique and the retype password matches with the 
	// originally typed password. Stores the user information in MD5 hash.
	func RegisterNewUser(w http.ResponseWriter, req *http.Request) {

		c, err := redis.Dial("tcp", ":6379")		
		if err != nil {
	    		log.Println("Error in connecting to REDIS Server ", err.Error())
	    		return
		}
		
		defer c.Close()
		
		userID := req.FormValue("userid")
		passWord := req.FormValue("password")
		retypePwd := req.FormValue("retypepassword")

		switch "" {
			
			case userID:

				t, err := template.ParseFiles("html/error.html")
				if err != nil {
					log.Println("Template parsing error ", err.Error())
					return
				}

				redirectPage := "Registration"
				redirectHtml := "register"
				errorMsg := "User ID cannot be empty..."

				n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
				t.Execute(w, n)

				return

			case passWord:
				
				t, err := template.ParseFiles("html/error.html")
				if err != nil {
					log.Println("Template parsing error ", err.Error())
					return
				}

				redirectPage := "Registration"
				redirectHtml := "register"
				errorMsg := "Password cannot be empty..."

				n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
				t.Execute(w, n)

				return				

		}

		pwdRegex := regexp.MustCompile(`[^a-zA-Z0-9]`)
		pwd := pwdRegex.Match([]byte(passWord))

		if pwd == true {
			
			t, err := template.ParseFiles("html/error.html")
			if err != nil {
				log.Println("Template parsing error ", err.Error())
				return
			}

			redirectPage := "Registration"
			redirectHtml := "register"
			errorMsg := "Password should include alphanumeric, uppercase and lowercase letters. Other characters are not allowed"

			n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
			t.Execute(w, n)

			return
			
		}
		
		repwd := pwdRegex.Match([]byte(retypePwd))

		if repwd == true || retypePwd != passWord {
	
			t, err := template.ParseFiles("html/error.html")
			if err != nil {
				log.Println("Template parsing error ", err.Error())
				return
			}

			redirectPage := "Registration"
			redirectHtml := "register"
			errorMsg := "Passwords did not match, try again..."

			n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
			t.Execute(w, n)
	
			return

		}
	
		userName := req.FormValue("username")
			
		keyMd5 := md5.New()
		io.WriteString(keyMd5, userID)

		newUserKey := fmt.Sprintf("%x", keyMd5.Sum(nil))

		pwdMd5 := md5.New()
		io.WriteString(pwdMd5, passWord)

		newUserPwd := fmt.Sprintf("%x", pwdMd5.Sum(nil))

		userExists, err := redis.Bool(c.Do("EXISTS", newUserKey))	
		if err != nil {
			log.Println("Error ",err.Error()," in querying REDIS Server.")
			return
		}

		if userExists == true {

			t, err := template.ParseFiles("html/error.html")
			if err != nil {
				log.Println("Template parsing error ", err.Error())
				return
			}

			redirectPage := "Registration"
			redirectHtml := "register"
			errorMsg := "User ID already exists, please choose a different ID..."

			n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
			t.Execute(w, n)

			return

		}

		accountCreated, err := c.Do("HSET", newUserKey, "Password", newUserPwd)
		if err != nil {
			log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
			return 
		}

		done, err := redis.Int(accountCreated, nil)
		if err != nil {
			log.Println("Error in converting to Integer ", err.Error())
			return 
		}

		if done == 1 {

			_, err := c.Do("HSET", newUserKey, "Name", userName)
			if err != nil {
				log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
				return 
			}

			log.Println("New user registered! Name: " + userName)

		}
	
		t, err := template.ParseFiles("html/redirection.html")
		if err != nil {
			log.Println("Template parsing error ", err.Error())
			return
		}

		n := &User{Action: "Registration"}
		t.Execute(w, n)
		
	}

	// Checks the sign-in form filled by the user by getting the 
	// hash value of the user id and matching the stored hashed
	// password with the entered password, which is converted into
	// hash brfore comapring. If the details matches, it allows access
	// else raises an error and tells to fill the form again.
	func SignInCheck(w http.ResponseWriter, req *http.Request) {

		c, err := redis.Dial("tcp", ":6379")
		if err != nil {
	    		log.Println("Error in connecting to REDIS Server ", err.Error())
	    		return
		}

		defer c.Close()
		
		userID := req.FormValue("userid")
		passWord := req.FormValue("password")

		switch "" {
			
			case userID:
				
				t, err := template.ParseFiles("html/error.html")
				if err != nil {
					log.Println("Template parsing error ", err.Error())
					return
				}

				redirectPage := "Sign-in"
				redirectHtml := "signin"
				errorMsg := "User ID cannot be empty..."

				n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
				t.Execute(w, n)

				return

			case passWord:
				
				t, err := template.ParseFiles("html/error.html")
				if err != nil {
					log.Println("Template parsing error ", err.Error())
					return
				}

				redirectPage := "Sign-in"
				redirectHtml := "signin"
				errorMsg := "Password cannot be empty..."

				n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
				t.Execute(w, n)

				return

		}
	
		keyMd5 := md5.New()
		io.WriteString(keyMd5, userID)

		userKey := fmt.Sprintf("%x", keyMd5.Sum(nil))

		pwdMd5 := md5.New()
		io.WriteString(pwdMd5, passWord)

		userPwd := fmt.Sprintf("%x", pwdMd5.Sum(nil))

		userExists, err := redis.Bool(c.Do("EXISTS", userKey))	
		if err != nil {
			log.Println("Error ",err.Error()," in querying REDIS Server.")
			return
		}

		if userExists == false {

			t, err := template.ParseFiles("html/error.html")
			if err != nil {
				log.Println("Template parsing error ", err.Error())
				return
			}

			redirectPage := "Sign-in"
			redirectHtml := "signin"
			errorMsg := "User ID does not exist, create a new account or enter correct details..."

			n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
			t.Execute(w, n)

			return

		}

		storedPwdHash, err := c.Do("HGET", userKey, "Password")
		if err != nil {
			log.Println("Error ", err.Error(), " in fetching value from REDIS. Quitting...")
			return
		}

		storedPwd, err := redis.String(storedPwdHash, nil) 
			if err != nil {
			log.Println("Error in converting to String ", err.Error())
			return 
		}
		

		if storedPwd != userPwd {
			
			t, err := template.ParseFiles("html/error.html")
				if err != nil {
					log.Println("Template parsing error ", err.Error())
					return
				}

				redirectPage := "Sign-in"
				redirectHtml := "signin"
				errorMsg := "Incorrect password, try again..."

				n := &User{Name: redirectPage, Action: redirectHtml, Error: errorMsg}
				t.Execute(w, n)

				return

		}

		nameHash, err := c.Do("HGET", userKey, "Name")
		if err != nil {
			log.Println("Error ", err.Error(), " in fetching value from REDIS. Quitting...")
			return
		}

		userName, err := redis.String(nameHash, nil)
		if err != nil {
			log.Println("Error in converting to String ", err.Error())
			return
		}

		log.Println(userName + " logged in to his/her account")

		AccountDisplay(w, req, userKey)

	}

	// Display handler for showing the account options the user
	// has with its account. It validates the user everytime it 
	// makes a choice using its user id hash.
	func AccountDisplay(w http.ResponseWriter, req *http.Request, userKey string) {

		c, err := redis.Dial("tcp", ":6379")
		if err != nil {
	    		log.Println("Error in connecting to REDIS Server ", err.Error())
	    		return
		}

		defer c.Close()

		nameHash, err := c.Do("HGET", userKey, "Name")
		if err != nil {
			log.Println("Error ", err.Error(), " in fetching value from REDIS. Quitting...")
			return
		}

		userName, err := redis.String(nameHash, nil)
		if err != nil {
			log.Println("Error in converting to String ", err.Error())
			return
		}

		t, err := template.ParseFiles("html/myaccount.html")
		if err != nil {
			log.Println("Template parsing error ", err.Error())
			return
		}

		n := &User{Name: userName, UserHash: userKey}
		t.Execute(w, n)
		
	}

	// Parses the data needed by the ShowHistory function to show
	// the user its history.
	func ParseHistoryData(locationData string) (string,string,string) {
	
		locRegex := regexp.MustCompile(`(\d+[NS][EW])\s+([A-Za-z_]+)`)
		
		historyData := locRegex.FindAllStringSubmatch(locationData, -1)

		latlong := historyData[0][1]
		action := historyData[0][2]

		intermediateData := []byte(latlong)

		latDeg := []byte{intermediateData[0], intermediateData[1], intermediateData[2]}
		latDec := []byte{intermediateData[3], intermediateData[4], intermediateData[5]}

		longDeg := []byte{intermediateData[6], intermediateData[7], intermediateData[8]}
		longDec := []byte{intermediateData[9], intermediateData[10], intermediateData[11]}

		direction := string([]byte{intermediateData[12], intermediateData[13]})

		var latString, longString string

		switch direction {
			
			case "NE":
				
				latString = fmt.Sprintf("%s.%s", latDeg, latDec)
				longString = fmt.Sprintf("%s.%s", longDeg, longDec)


			case "NW":

				latString = fmt.Sprintf("%s.%s", latDeg, latDec)
				longString = fmt.Sprintf("-%s.%s", longDeg, longDec)

			case "SE":

				latString = fmt.Sprintf("-%s.%s", latDeg, latDec)
				longString = fmt.Sprintf("%s.%s", longDeg, longDec)

			case "SW":

				latString = fmt.Sprintf("-%s.%s", latDeg, latDec)
				longString = fmt.Sprintf("-%s.%s", longDeg, longDec)

			}

		return latString, longString, action
	
	}		

	// Shows the user its history by retrieving the page
	// with the user's hash key.
	func ShowHistory(w http.ResponseWriter, req *http.Request) {

		userHash := req.FormValue("userhash")
		if userHash == "" {

			http.ServeFile(w, req, "html/denyaccess.html")
			return
		}

		RetrieveHistory(w, userHash)
	
	}

	// Retrieves the user's history by generating a template
	// of the history page and the redirects the user might need
	// like New Search and Sign-out. It searches the user history
	// using its hash key and parses the data it receives from
	// the database and shows the data in an ordered list. If 
	// the history is empty then it just shows the redirects.
	func RetrieveHistory(w http.ResponseWriter, userHash string) {

		const history = 
		`<!DOCTYPE html>
		<html>
		<body>
		<p><ol>{{.}}</ol></p>
		</body>
		</html>`

		const options =
		`<!DOCTYPE html>
		<html>
		<body>
		<h3>My Location History</h3>
		<form name="search" action="accountaction.html" method="post">
		<button name="account" type="submit" value="search">New Search</button>
		<input name="userhash" type="hidden" value={{.}} />
		</form><br/>
		<form name="signout" action="accountaction.html" method="post">
		<button name="account" type="submit" value="signout">Sign Out</button>
		<input name="userhash" type="hidden" value={{.}} />
		</form>
		</body>
		</html>`

		c, err := redis.Dial("tcp", ":6379")
		if err != nil {
	    		log.Println("Error in connecting to REDIS Server ", err.Error())
	    		return
		}

		defer c.Close()

		doubleHash := md5.New()
		io.WriteString(doubleHash, userHash)

		doubleHashKey := fmt.Sprintf("%x", doubleHash.Sum(nil))

		keyExists, err := redis.Bool(c.Do("EXISTS", doubleHashKey))
		if err != nil {
			log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
			return 
		}

		if keyExists != true {
				
			t := template.Must(template.New("options.html").Parse(options))
			safe := template.HTMLEscapeString(userHash)
			t.Execute(w, template.HTML(safe))
			return
			
		} 
		
		getLocs, err := c.Do("LRANGE", doubleHashKey, "0", "-1")
		if err != nil {
			log.Println("Error ", err.Error(), " in fetching value from REDIS. Quitting...")
			return
		}
		
		getLocsString, err := redis.Strings(getLocs, nil)
		if err != nil {
			log.Println("Error ", err.Error())
			return
		}

		var historyFormat []byte

		for _, locs := range getLocsString {
			
			latitude, longitude, action := ParseHistoryData(locs)

			var selection string
			switch action {

				case "maps": selection = "Google Maps"
		
				case "wiki": selection = "Wikipedia"

				case "flickr": selection = "Flickr Maps"

				case "flickr_nearby": selection = "Flickr Nearby"

				case "address": selection = "Where am I?"

				case "resource": selection = "I'm Feeling Lucky!"

				default: selection = "None"

			}
			
			historyString := ("\a Latitude: " + latitude + "\n" + "Longitude: " + longitude + "\n" + "Searched: " + selection + "\n\n \b")
			
			historyFormat = append(historyFormat, []byte(historyString)...)
		}

		t := template.Must(template.New("options.html").Parse(options))
		safe := template.HTMLEscapeString(userHash)
		t.Execute(w, template.HTML(safe))

		t = template.Must(template.New("history.html").Parse(history))
		safe = template.HTMLEscapeString(string(historyFormat))
		safe = strings.Replace(safe, "\n", "<br>", -1)
		safe = strings.Replace(safe, "\a", "<li>", -1)
		safe = strings.Replace(safe, "\b", "</li>", -1)
		t.Execute(w, template.HTML(safe)) 

		return
		
	}
	
	// Processes the user's request to search about a location.
	// It first saves the location entered in the database to 
	// let the user view its history later on. Then, it passes
	// the control to ProcessForm to handle the request further.
	func ProcessUserForm(w http.ResponseWriter, req *http.Request) {

		const serverError =
		`<!DOCTYPE html>
		<html>
		<body>
		<p>{{.}}</p>
		</body>
		</html>`

		c, err := redis.Dial("tcp", ":6379")
		if err != nil {
	    		log.Println("Error in connecting to REDIS Server ", err.Error())
	    		return
		}

		defer c.Close()

		lat := req.FormValue("latitude")
		
		long := req.FormValue("longitude")
		
		userHash := req.FormValue("userhash")

		action := req.FormValue("select")

		if userHash == "" {

			http.ServeFile(w, req, "html/denyaccess.html")
			return
		}

		if lat == "" || long == "" {
	
			errorMsg := "Not enough data to process request"

			t := template.Must(template.New("nodata.html").Parse(serverError))
			safe := template.HTMLEscapeString(errorMsg)
			t.Execute(w, template.HTML(safe))

			return
		
		}

		latitude := ParseCoordinate(lat, "latitude")
		longitude := ParseCoordinate(long, "longitude")

		latlngKey := GetKey(latitude, longitude, "full")
		if latlngKey == "null" {

			errorMsg := "Internal Server Error. Please choose a different location..."
			
			t := template.Must(template.New("internalerror.html").Parse(serverError))
			safe := template.HTMLEscapeString(errorMsg)
			t.Execute(w, template.HTML(safe))

			return
		
		}
	
		latlngKey = fmt.Sprintf("%s %s", latlngKey, action)
		
		doubleHash := md5.New()
		io.WriteString(doubleHash, userHash)

		doubleHashKey := fmt.Sprintf("%x", doubleHash.Sum(nil))

		_, err = c.Do("RPUSH", doubleHashKey, latlngKey)
		if err != nil {
			log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
			return 
		}

		ProcessForm(w, req)

	}

	// Processes the search form by parsing the field values
	// entered by the user and the action selected. Redirects
	// the user to the appropriate page if the request is 
	// error free.
	func ProcessForm(w http.ResponseWriter, req *http.Request) {

		const addressTempl = 
		`<!DOCTYPE html>
		<html>
		<body>
		<h3>YOU ARE HERE:</h3>{{.}}
		</body>
		</html>`

		const serverError =
		`<!DOCTYPE html>
		<html>
		<body>
		<p>{{.}}</p>
		</body>
		</html>`
		
		lat := req.FormValue("latitude")

		long := req.FormValue("longitude")
		
		action := req.FormValue("select")
		
		if lat == "" || long == "" {

			errorMsg := "Not enough data to process request"

			t := template.Must(template.New("nodata.html").Parse(serverError))
			safe := template.HTMLEscapeString(errorMsg)
			t.Execute(w, template.HTML(safe))

			return

		}
		
		latitude := ParseCoordinate(lat, "latitude")
		longitude := ParseCoordinate(long, "longitude")

		log.Println(fmt.Sprintf("Received latitude value = %d.%d %s", latitude.degrees, latitude.decimal, latitude.direction))
		log.Println(fmt.Sprintf("Received longitude value = %d.%d %s", longitude.degrees, longitude.decimal, longitude.direction))
		
		log.Println("Action selected by user: " + action)
		
		rID := GetKey(latitude, longitude, "full")
		if rID == "null" {
			
			errorMsg := "Internal Server Error. Please choose a different location..."

			t := template.Must(template.New("internalerror.html").Parse(serverError))
			safe := template.HTMLEscapeString(errorMsg)
			t.Execute(w, template.HTML(safe))

			return

		}
		
		log.Println("Full key is: " + rID)
	
		switch action {
		
			case "maps": 

				mapsUrl := "http://www.maps.google.com/?q=" + lat + "," + long

				log.Println("Redirecting user to Google Maps...\n")

				http.Redirect(w, req, mapsUrl, http.StatusFound)

			case "wiki":	
				
				wikiUrl := GetResource(rID, action)
			
				if wikiUrl == "Error" {
					
					errorMsg := "Database Error. Please choose a different location..."
			
					t := template.Must(template.New("wikierror.html").Parse(serverError))
					safe := template.HTMLEscapeString(errorMsg)
					t.Execute(w, template.HTML(safe))

					return

				}

				log.Println("Redirecting user to Wikipedia Link...\n")

				http.Redirect(w, req, wikiUrl, http.StatusFound)
			
			case "address":

				var address string

				mapsApi := "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long + "&sensor=true"

				var jsondata MapData
	
				jsondata = UnmarshalJsonData(mapsApi)
				if jsondata.StatusCode == "ERROR" {
					return
				}

				for ; jsondata.StatusCode == "OVER_QUERY_LIMIT" ; {

					time.Sleep(1*1e9)
		
					jsondata = UnmarshalJsonData(mapsApi)
				}

	
				switch jsondata.StatusCode {
		
					case "ZERO_RESULTS":

					log.Println("User at Unknown Location, can't serve address...")
					log.Println(jsondata.StatusCode)

					address = "Unknown Location"

					case "OK":

					address = fmt.Sprintf("\t%v\n", jsondata.Data[0].FormattedAdd)

					log.Println("Redirecting user to show it's address...\n") 
	
				}	

				t := template.Must(template.New("address.html").Parse(addressTempl))
				safe := template.HTMLEscapeString(address)
				t.Execute(w, template.HTML(safe))

				return
		
			case "flickr":
			
				flickrMapsUrl := "http://www.flickr.com/map/?fLat=" + lat + "&fLon=" + long

				log.Println("Redirecting user to Flickr Maps...\n")

				http.Redirect(w, req, flickrMapsUrl, http.StatusFound)
			
			case "flickr_nearby":
			
				flickrNearbyUrl := "http://www.flickr.com/nearby/" + lat + "," + long + "?show=thumb&fromfilter=1&by=everyone&taken=alltime&sort=mostrecent"

				log.Println("Redirecting user to Flickr Nearby...\n")

				http.Redirect(w, req, flickrNearbyUrl, http.StatusFound)
				
			case "resource":
				
				resourceName := GetResource(rID, action)
				
				if resourceName == "Error" {

					errorMsg := "Database Error. Please choose a different location..."
			
					t := template.Must(template.New("resourceerror.html").Parse(serverError))
					safe := template.HTMLEscapeString(errorMsg)
					t.Execute(w, template.HTML(safe))

					return

				}
				
				log.Println("Serving resource file to user...")
				
				fileName := "html/" + resourceName
				
				http.ServeFile(w, req, fileName)
			
			default:
	
				errorMsg := "Database Error. Please choose a different location..."
			
				t := template.Must(template.New("defaulterror.html").Parse(serverError))
				safe := template.HTMLEscapeString(errorMsg)
				t.Execute(w, template.HTML(safe))

				return

		}
	}

	// Unmarshals the JSON data received from Google
	// Maps and returns the struct variable back after
	// unmarshalling. It returns error in statuscode
	// if any problem in handling the request occurs.
	func UnmarshalJsonData (mapsApi string) MapData {

		resp, err := http.Get(mapsApi)
		var jsondata MapData
				
		if err != nil {
			log.Println("Fatal rror ", err.Error())
			jsondata.StatusCode = "ERROR"
			return jsondata
		}
		
		respData, err := ioutil.ReadAll(resp.Body)
		
		if err != nil {
			log.Println("Fatal error ", err.Error())
			jsondata.StatusCode = "ERROR"
			return jsondata
		}
		
		jsonErr := json.Unmarshal([]byte(respData), &jsondata)
		
		if jsonErr != nil {
			log.Println("Fatal error ", err.Error())
			jsondata.StatusCode = "ERROR"
			return jsondata
		}

		return jsondata

	}

	// Parses the coordiantes that the user entered by using
	// a regexp and enters the parsed coordinates into a
	// srtuct and returns that struct.
	func ParseCoordinate(coord string, typeCoord string) coordinate {
		
		var finalCoord coordinate
		var err error
		var myRegex = regexp.MustCompile(`(-?)(\d+)\.(\d+)`)

				
		str := myRegex.FindAllStringSubmatch(coord, -1)
		
		if str[0][1] == "-" && typeCoord == "latitude" {

			finalCoord.direction = "S"
 
		} else if str[0][1] != "-" && typeCoord == "latitude" {

			finalCoord.direction = "N"

		} else if str[0][1] != "-" && typeCoord == "longitude" {

			finalCoord.direction = "E"

		} else if str[0][1] == "-" && typeCoord == "longitude" {

			finalCoord.direction = "W"

		}
		
		finalCoord.degrees, err = strconv.ParseInt(str[0][2], 10, 64)
		finalCoord.decimal, err = strconv.ParseInt(str[0][3], 10, 64)
		
		if err !=nil {
			log.Println("String to integer conversion failed.")
		}
		
		return finalCoord
	}
	
	// Gets the proper URL from the received key by 
	// checking if the key exists in the database or
	// not. If the key exists, it sends the relevant
	// URL and if it doesn't, it tries to find the
	// nearest location around the user and send the
	// relevant URL of the nearest location
	func GetResource(key string, action string) string {
	
		c, err := redis.Dial("tcp", ":6379")
		if err != nil {
	    		log.Println("Error in connecting to REDIS Server ", err.Error())
	    		return "Error";
		}
		
		defer c.Close()
		
		keyExists, err := redis.Bool(c.Do("EXISTS", key))	
		
		if err != nil {
			log.Println("Error ",err.Error()," in querying REDIS Server.")
			return "Error"
		}
		
		switch keyExists {
			
			case true:
			
				switch action {
			
					case "wiki":

						urlData, err := c.Do("LINDEX", key, "0")
						if err != nil {
							log.Println("Error ",err.Error()," in querying REDIS Server.")
							return "Error"
						}
	
						wikiUrl, err := redis.String(urlData, nil)
						if err != nil {
							log.Println("Error ", err.Error())
							return "Error"
						}
	
						return wikiUrl
					
					case "resource":
						
						redisData, err := c.Do("LINDEX", key, "1")
						if err != nil {
							log.Println("Error ",err.Error()," in querying REDIS Server.")
							return "Error"
						}
	
						fileName, err := redis.String(redisData, nil)
						if err != nil {
							log.Println("Error ", err.Error())
							return "Error"
						}
	
						return fileName
			
				}

			case false:

				log.Println("Couldn't find data in the database for the received key")

				nearestKey, err := FindNearestLocation(key)
				if err != 1 {
						log.Println("Error in locating key")
						return "Error"
				}
		
				switch action {
			
					case "wiki":

						urlData, err := c.Do("LINDEX", nearestKey, "0")
						if err != nil {
							log.Println("Error ",err.Error()," in querying REDIS Server.")
							return "Error"
						}
	
						wikiUrl, err := redis.String(urlData, nil)
						if err != nil {
							log.Println("Error ", err.Error())
							return "Error"
						}
	
						return wikiUrl
						
					case "resource":
						
						redisData, err := c.Do("LINDEX", nearestKey, "1")
						if err != nil {
							log.Println("Error ",err.Error()," in querying REDIS Server.")
							return "Error"
						}
	
						fileName, err := redis.String(redisData, nil)
						if err != nil {
							log.Println("Error in string conversion ", err.Error())
							return "Error"
						}
	
						return fileName
			
				}
			
		} 
		
		return "null"
	} 
	
	// Returns the key out of the received coordinates
	// which can be either full key or part key decided
	// by the command received
	func GetKey(latitude coordinate, longitude coordinate, keyType string) string {
	
		latDegStr := strconv.Itoa(int(latitude.degrees))
		latDecStr := strconv.Itoa(int(latitude.decimal))
		
		longDegStr := strconv.Itoa(int(longitude.degrees))
		longDecStr := strconv.Itoa(int(longitude.decimal))
		
		switch len(latDegStr) {
			 		
			case 1: 
				latDegStr = "00" + latDegStr

			case 2:
				latDegStr = "0" + latDegStr
					
		}
		
		switch len(latDecStr) {
			
			case 1: 			
				latDecStr = latDecStr + "00"
	
			case 2:
				latDecStr = latDecStr + "0" 
			
		}
		
		switch len(longDegStr) {
			 		
			case 1: 
				longDegStr = "00" + longDegStr

			case 2:
				longDegStr = "0" + longDegStr
					
		}
		
		switch len(longDecStr) {
			
			case 1: 			
				longDecStr = longDecStr +"00"
	
			case 2:
				longDecStr = longDecStr + "0"
			
		}

		switch keyType {

			case "full":
				key := latDegStr + latDecStr + longDegStr + longDecStr + latitude.direction + longitude.direction
				return key
			 
			case "part":
				key := latDegStr + longDegStr + latitude.direction + longitude.direction
				return key
			
			default:
				return "null"
			
		}
		
		return "null"
	}

	// Returns the key to check in the database for the 
	// nearest location the user might be. It searches the
	// all the available locations near the user and sends
	// the one which is nearest to it.
	func FindNearestLocation(receivedKey string) (string, int) {
		
		c, err := redis.Dial("tcp", ":6379")
		if err != nil {
    			log.Println("Error in connecting to REDIS Server ", err.Error())
				return "null", 0
		}
		
		defer c.Close()
	
		log.Println("Finding the nearest location matching the received co-ordinates...")

		intermediateKey := []byte(receivedKey)

		latDeg := []byte{intermediateKey[0], intermediateKey[1], intermediateKey[2]}
		latDec := []byte{intermediateKey[3], intermediateKey[4], intermediateKey[5]}

		longDeg := []byte{intermediateKey[6], intermediateKey[7], intermediateKey[8]}
		longDec := []byte{intermediateKey[9], intermediateKey[10], intermediateKey[11]}

		direction := string([]byte{intermediateKey[12], intermediateKey[13]})

		var latString, longString string

		switch direction {
			
			case "NE":
				
				latString = fmt.Sprintf("%s.%s", latDeg, latDec)
				longString = fmt.Sprintf("%s.%s", longDeg, longDec)


			case "NW":

				latString = fmt.Sprintf("%s.%s", latDeg, latDec)
				longString = fmt.Sprintf("-%s.%s", longDeg, longDec)

			case "SE":

				latString = fmt.Sprintf("-%s.%s", latDeg, latDec)
				longString = fmt.Sprintf("%s.%s", longDeg, longDec)

			case "SW":

				latString = fmt.Sprintf("-%s.%s", latDeg, latDec)
				longString = fmt.Sprintf("-%s.%s", longDeg, longDec)

			}

		latitude := ParseCoordinate(latString, "latitude")
		longitude := ParseCoordinate(longString, "longitude")

		lookUpKey := GetKey(latitude, longitude, "part")

		log.Println("Lookup Key is: " + lookUpKey)

		keyExists, err := redis.Bool(c.Do("EXISTS", lookUpKey))
		if err != nil {
				log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
				return "null", 0
		}

		if keyExists != true {
			log.Println("Unknown Location...")
			return "Key Not Found", -1
		} 
		
		getData, err := c.Do("LRANGE", lookUpKey, "0", "-1")
		if err != nil {
			log.Println("Error ", err.Error(), " in fetching value from REDIS. Quitting...")
			return "null", 0
		}
		
		getDataString, err := redis.Strings(getData, nil)
		if err != nil {
			log.Println("Error in string conversion ", err.Error())
			return "null", 0
		}

		log.Println("Found Nearest Location List...")

		decString := fmt.Sprintf("%s%s", latDec, longDec)
		decFloat, _ := strconv.ParseFloat(decString, 64)

		diffDecData := make([]float64, len(getDataString))

		var minDiffIndex int

		for i, dataString := range getDataString {

			getDataFloat, _ := strconv.ParseFloat(dataString, 64)
			diffDecData[i] = math.Abs(decFloat - getDataFloat)
			
			if i>0 {
				minDiffIndex = FindMin(minDiffIndex, i, diffDecData)
			} else {	
				minDiffIndex = i
			}
		}
		
		nearestLatLong := getDataString[minDiffIndex]

		intermediateKey = []byte(nearestLatLong)

		latDec = []byte{intermediateKey[0], intermediateKey[1], intermediateKey[2]}
		longDec = []byte{intermediateKey[3], intermediateKey[4], intermediateKey[5]}

		fullKey := fmt.Sprintf("%s%s%s%s%s", latDeg, latDec, longDeg, longDec, direction)
		log.Println("Sending Nearest Location Key: " + fullKey)

	return fullKey, 1

	}
		
	// Finds the minimum number out of two numbers and returns 
	// the index of the minimum number. The function takes in
	// the previous minimum index and the current index and
	// the float array from which the minimum number is to be
	// found out.
	func FindMin(minDiffIndex int, i int, diffDecData []float64) int {
		
		temp := math.Min(diffDecData[minDiffIndex], diffDecData[i])

		switch temp {
			
			case diffDecData[minDiffIndex]:
				return minDiffIndex
				
			default:
				return i
				
		}

		return i
		
	}

	// readLines takes in filename or the path to the filename
	// as string and gives the contents of the file in a string
	// array. It returns an error if it fails to read the file 
	// correctly.
	func ReadLines(path string) (lines []string, err error) {
		
		var (
			file *os.File
			part []byte
			prefix bool
		)
	
		if file, err = os.Open(path); err != nil {
			return
		}
	
		
		reader := bufio.NewReader(file)
		buffer := bytes.NewBuffer(make([]byte, 0))
		
		for {
				if part, prefix, err = reader.ReadLine(); err != nil {
		    		break
				}
			
		buffer.Write(part)
			if !prefix {
			    lines = append(lines, buffer.String())
			    buffer.Reset()
			}
		}
		
		if err == io.EOF {
			err = nil
		}
		
		file.Close()
		return
	}

	// Sets up the whole database from an external file
	// after flushing any previous databases, if any.
	// Parses the databse first by using a regexp and
	// pushes full key as well as part key in the database.
	// Returns 1 for success and 0 for failure.
	func SetupRedis(filename string) int {
		
		c, err := redis.Dial("tcp", ":6379")
		if err != nil {
    		log.Println("Error in connecting to REDIS Server ", err.Error())
		}

		defer c.Close()

		
		log.Println("Flushing All Previous Databases")
		
		_, err = c.Do("FLUSHALL")
			if err != nil {
				log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
				return 0
			}

		lines, err := ReadLines(filename)
		
		if err != nil {
			log.Println("Error in reading file " + filename + " ", err.Error())
			return 0
		}
		
		strRegex := regexp.MustCompile(`(\d+[NS][EW])\s+([A-Za-z0-9.:/_%(),]+)\s+([a-zA-Z.]+)`)
		
		var fullKeyCount, partKeyCount int

		for _, line := range lines {
			
			str := strRegex.FindAllStringSubmatch(line, -1)
			
			firstValueHead := str[0][2]
			firstValueTail := str[0][3]
			firstKey := str[0][1]

			fullKeyExists, err := redis.Bool(c.Do("EXISTS", firstKey))
			if err != nil {
				log.Println("Error ", err.Error())
			}

			if fullKeyExists != true {
				fullKeyCount++
			}

			_, err = c.Do("RPUSH", firstKey, firstValueHead)
			if err != nil {
				log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
				return 0
			}
			
			_, err = c.Do("RPUSH", firstKey, firstValueTail)
			if err != nil {
				log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
				return 0
			}
						
			latLong := []byte(str[0][1])
			
			latDeg := []byte{latLong[0], latLong[1], latLong[2]}
			latDec := []byte{latLong[3], latLong[4], latLong[5]}
			
			latDegStr := string(latDeg)
			latDecStr := string(latDec)
			
			longDeg := []byte{latLong[6], latLong[7], latLong[8]}
			longDec := []byte{latLong[9], latLong[10], latLong[11]}						

			longDegStr := string(longDeg)
			longDecStr := string(longDec)

			dir := []byte{latLong[12], latLong[13]} 
			 
			direction := string(dir)

			secondKey := latDegStr + longDegStr + direction
			secondValue := latDecStr + longDecStr
			
			partKeyExists, err := redis.Bool(c.Do("EXISTS", secondKey))
			if err != nil {
				log.Println("Error ", err.Error())
			}

			if partKeyExists != true {
				partKeyCount++
			}

			_, err = c.Do("RPUSH", secondKey, secondValue) 
			if err != nil {
				log.Println("Error ", err.Error(), " in pushing value to REDIS. Quitting...")
				return 0
			}
			
		}

		log.Println(fmt.Sprintf("Pushed %d Full Keys and %d Partial Keys to Redis...", fullKeyCount, partKeyCount))
		
		return 1
	}
	
	// Declared all the necessary handlers and start the
	// server. Redirects to the proper function if a
	// particular handler is called. Also, server can 
	// be started in HTTPS mode or HTTP mode. HTTPS mode
	// uses the certificate and key provided by the 
	// generateCert.go program. Exits the main function
	// if it cannot establish a connection on the port.
	func main() {
		http.HandleFunc("/", GetUserInput)

		http.HandleFunc("/splashselect.html", RedirectUser)

		http.HandleFunc("/newuser.html", RegisterNewUser)

		http.HandleFunc("/checksignin.html", SignInCheck)
	
		http.HandleFunc("/accountaction.html", RedirectFromAccount)

		http.HandleFunc("/data.html", ProcessForm)
		http.HandleFunc("/userdata.html", ProcessUserForm)

		http.HandleFunc("/myhistory.html", ShowHistory)

		http.HandleFunc("/myaccount.html", DenyAccess)
		http.HandleFunc("/user.html", DenyAccess)
		http.HandleFunc("/redirection.html", DenyAccess)	
		
		succ := SetupRedis("datastore/database.txt")
		
		if succ != 1 {
			log.Println("Failed to setup Redis. Quitting...")
			os.Exit(succ)
		}
		
		err := http.ListenAndServeTLS(httpPort, "cert.pem", "key.pem", nil)
		//err := http.ListenAndServe(httpPort, nil)
		if err != nil {
			log.Println("ListenAndServe Error ", err.Error())
			os.Exit(1)
		}
	}
